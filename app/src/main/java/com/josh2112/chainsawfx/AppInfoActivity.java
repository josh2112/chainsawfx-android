package com.josh2112.chainsawfx;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class AppInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_app_info );

        ((TextView)findViewById( R.id.text_credits_chainsaw )).setMovementMethod( LinkMovementMethod.getInstance() );
    }
}
