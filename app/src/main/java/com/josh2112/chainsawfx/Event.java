package com.josh2112.chainsawfx;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Joshua Foster.
 */
public class Event<T> {

    public interface Listener<T> {
        void onEvent( T data );
    }

    private List<Listener<T>> listeners = new ArrayList<>();

    public void addListener( Listener<T> listener ) {
        listeners.add( listener );
    }

    public void removeListener( Listener<T> listener ) {
        listeners.remove( listener );
    }

    public void fireEvent( T data ) {
        for( Listener<T> listener : listeners ) {
            listener.onEvent( data );
        }
    }
}
