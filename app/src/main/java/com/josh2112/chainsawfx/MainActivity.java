package com.josh2112.chainsawfx;

import android.content.Intent;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.SoundPool;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ProgressBar;

import java.util.Arrays;
import java.util.Collections;

import io.codetail.animation.SupportAnimator;
import io.codetail.animation.ViewAnimationUtils;
import me.zhanghai.android.materialprogressbar.MaterialProgressBar;

public class MainActivity extends AppCompatActivity {

    public enum State {
        Idling,
        Accelerating,
        FullThrottle,
        Decelerating,
    }

    private static final String TAG = MainActivity.class.getSimpleName();

    private View loadingPanel, idlePanel, fullThrottlePanel;
    private ProgressBar progressBar;

    boolean isForeground, loadingFinished;
    State state;

    private SoundPool soundPool;
    SoundEffect idleSound, accelSound, fullThrottleSound, decelSound;

    Point lastTapPoint;

    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );

        progressBar = (ProgressBar)findViewById( R.id.progressbar );
        loadingPanel = findViewById( R.id.panel_loading );
        idlePanel = findViewById( R.id.panel_idle );
        fullThrottlePanel = findViewById( R.id.panel_fullThrottle );

        loadingPanel.setVisibility( View.VISIBLE );
        idlePanel.setVisibility( View.INVISIBLE );
        fullThrottlePanel.setVisibility( View.INVISIBLE );


        progressBar.setMax( 4 );

        findViewById( R.id.view_throttle ).setOnTouchListener( new View.OnTouchListener() {
            @Override
            public boolean onTouch( View v, MotionEvent event ) {
                switch( event.getAction() ) {
                    case MotionEvent.ACTION_DOWN:
                        lastTapPoint = new Point( (int) event.getX(), (int) event.getY() );
                        animateShowFullThrottlePanel();
                        doAccel();
                        return true;

                    case MotionEvent.ACTION_UP:
                        animateHideFullThrottlePanel();
                        doDecel();
                        return true;

                    default:
                        return false;
                }
            }
        } );

        findViewById( R.id.image_info ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick( View v ) {
                startActivity( new Intent( MainActivity.this, AppInfoActivity.class ) );
            }
        } );

        soundPool = new SoundPool( 1, AudioManager.STREAM_MUSIC, 1 );
        soundPool.setOnLoadCompleteListener( new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete( SoundPool soundPool, int sampleId, int status ) {
                if( status == 0 ) progressBar.setProgress( progressBar.getProgress() + 1 );
                if( progressBar.getProgress() == progressBar.getMax() ) {
                    doLoadedAnimation();
                }
            }
        } );

        idleSound = new SoundEffect( this, soundPool, R.raw.chainsaw_idle, true );
        accelSound = new SoundEffect( this, soundPool, R.raw.chainsaw_accel );
        fullThrottleSound = new SoundEffect( this, soundPool, R.raw.chainsaw_high, true );
        decelSound = new SoundEffect( this, soundPool, R.raw.chainsaw_decel );

        accelSound.getPlaybackFinishedEvent().addListener( new Event.Listener<SoundEffect>() {
            @Override
            public void onEvent( SoundEffect data ) {
                if( state == State.Accelerating ) doFullThrottle();
            }
        } );
        decelSound.getPlaybackFinishedEvent().addListener( new Event.Listener<SoundEffect>() {
            @Override
            public void onEvent( SoundEffect data ) {
                if( state == State.Decelerating ) doIdle();
            }
        } );
    }

    private void doLoadedAnimation() {

        idlePanel.setVisibility( View.VISIBLE );

        loadedAnimator = ViewAnimationUtils.createCircularReveal( idlePanel,
                idlePanel.getWidth()/2, idlePanel.getHeight()/2, 0,
                Math.max( idlePanel.getWidth(), idlePanel.getHeight() )/2 );
        loadedAnimator.setDuration( 500 );
        loadedAnimator.start();

        loadingFinished = true;
        doIdle();
    }

    private SupportAnimator animator, loadedAnimator;

    private void animateShowFullThrottlePanel() {
        if( animator != null && animator.isRunning() ) {
            animator.cancel();
        }

        fullThrottlePanel.setVisibility( View.VISIBLE );

        animator = makeRevealAnimation();
        animator.setDuration( accelSound.getDuration() );
        animator.start();
    }

    private void animateHideFullThrottlePanel() {
        if( animator != null && animator.isRunning()  ) {
            animator.cancel();
        }

        animator = makeRevealAnimation();
        animator = animator.reverse();
        animator.setDuration( decelSound.getDuration() );

        animator.addListener( new AnimatorAdapter() {
            boolean ignoreOnAnimationEnd = false;


            @Override
            public void onAnimationEnd() {
                if( !ignoreOnAnimationEnd ) {
                    fullThrottlePanel.setVisibility( View.INVISIBLE );
                } else ignoreOnAnimationEnd = false;
            }

            @Override
            public void onAnimationCancel() {
                ignoreOnAnimationEnd = true;
            }
        } );
        animator.start();
    }

    private SupportAnimator makeRevealAnimation() {
        int endRadius = Collections.max( Arrays.asList(
                distance( lastTapPoint, 0, 0 ),
                distance( lastTapPoint, 0, fullThrottlePanel.getHeight()),
                distance( lastTapPoint, fullThrottlePanel.getWidth(), fullThrottlePanel.getHeight() ),
                distance( lastTapPoint, fullThrottlePanel.getWidth(), 0 ) ) );
        return ViewAnimationUtils.createCircularReveal( fullThrottlePanel,
                lastTapPoint.x, lastTapPoint.y, 0, endRadius );
    }

    private int distance( Point pt, int x, int y ) {
        return (int)Math.sqrt( (pt.x-x)*(pt.x-x) + (pt.y-y)*(pt.y-y) );
    }

    @Override
    protected void onResume() {
        super.onResume();
        isForeground = true;
        if( loadingFinished ) doIdle();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isForeground = false;
        soundPool.autoPause();
    }

    private void doIdle() {
        if( isForeground ) idleSound.play();
        setState( State.Idling );
    }

    private void doAccel() {
        if( isForeground ) accelSound.play();
        setState( State.Accelerating );
    }

    private void doFullThrottle() {
        if( isForeground ) fullThrottleSound.play();
        setState( State.FullThrottle );
    }

    private void doDecel() {
        if( isForeground ) decelSound.play();
        setState( State.Decelerating );
    }

    private void setState( State state ) {
        this.state = state;
        Log.v( TAG, "State -> " + state );
    }

    public class AnimatorAdapter implements SupportAnimator.AnimatorListener {

        @Override
        public void onAnimationStart() {

        }

        @Override
        public void onAnimationEnd() {

        }

        @Override
        public void onAnimationCancel() {

        }

        @Override
        public void onAnimationRepeat() {

        }
    }
}
