package com.josh2112.chainsawfx;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.os.Handler;

/**
 * @author Joshua Foster.
 */
public class SoundEffect {

    private static final int NO_LOOP = 0;
    private static final int LOOP_FOREVER = -1;


    private SoundPool soundPool;

    private int soundId;
    private int duration;
    private boolean loop;

    private int streamId;

    private Event<SoundEffect> playbackFinishedEvent = new Event<>();

    public SoundEffect( Context context, SoundPool soundPool, int resId ) {
        this( context, soundPool, resId, false );
    }

    public SoundEffect( Context context, SoundPool soundPool, int resId, boolean loop ) {
        this.soundPool = soundPool;
        this.loop = loop;
        duration = findSoundDuration( context, resId );
        soundId = soundPool.load( context, resId, 1 );
    }

    public void play() {
        streamId = soundPool.play( soundId, 1, 1, 1, loop ? LOOP_FOREVER : NO_LOOP, 1 );
        if( !loop ) {
            Handler h = new Handler();
            h.postDelayed( new Runnable() {
                @Override
                public void run() {
                    playbackFinishedEvent.fireEvent( SoundEffect.this );
                }
            }, duration );
        }
    }

    public void stop() {
        soundPool.stop( streamId );
    }

    public Event<SoundEffect> getPlaybackFinishedEvent() {
        return playbackFinishedEvent;
    }

    private static int findSoundDuration( Context context, int resId ) {
        MediaPlayer tmp = MediaPlayer.create( context, resId );
        int duration = tmp.getDuration();
        tmp.reset();
        tmp.release();
        return duration;
    }

    public int getDuration() {
        return duration;
    }
}
